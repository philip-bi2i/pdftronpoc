## PDFTron SDK OCR Module

This module allows optical character recognition operations within PDFTron SDK. Specifically, the functionality within the `PDF.OCRModule` class relies on this package.

### Installation

This archive should be expanded on top of an existing PDFTron SDK package (version 7.0 or later). This will place the module itself in the correct location, as well as additional test files required to run the OCR conversion sample (located at `Samples/OCRTest/` in the original download package).

### Usage 

Within your own application logic, call `PDFNet.AddResourceSearchPath()` with the path to the location of `Lib/OCR.module` from this package, if this has been done correctly, then `PDF.OCRModule.IsModuleAvailable()` will return true, and the module will be operational.
