
# u can add the following line to integrate PDFNetPython3
# into your solution from anywhere on your system so long as
# the library was installed successfully via pip

from PDFNetPython3 import *

def main():

    # You need to initialize the PDFNet library 
    # Before calling any PDF related methods
    PDFNet.Initialize()

    # This example creates a new document
    # and a new page, then adds the page
    # in the page sequence of the document
    doc = PDFDoc()

    page1 = doc.PageCreate()
    doc.PagePushBack(page1)

    # We save the document in a linearized
    # format which is the most popular and 
    # effective way to speed up viewing PDFs
    doc.Save(("linearized_output.pdf"), SDFDoc.e_linearized)

    doc.Close()

if __name__ == "__main__":
    main()

