## PDFTron SDK AdvancedImaging Module

This module supports converting .dcm and other file formats to pdf using the `PDF.Convert.FromDICOM` method and the `PDF.Convert.ToPDF` method.

### Installation

This archive should be expanded on top of an existing PDFTron SDK package (version 9.0 or later). This will place the module itself in the correct location, as well as additional test files required to run the DICOM conversion sample (located at `Samples/AdvancedImagingTest/` in the original download package).

### Usage 

Within your own application logic, call `PDFNet.AddResourceSearchPath()` with the path to the location of `Lib/AdvancedImaging.module` from this package, if this has been done correctly, then `PDF.AdvancedImagingModule.IsModuleAvailable()` will return true, and `PDF.Convert.FromDICOM` will be operational.
