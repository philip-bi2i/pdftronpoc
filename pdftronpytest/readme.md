### To Run Python Samples:

- Prerequisite -
    A windows/linux system with python installed.
    - Download and install Python 
        e.g. similar to first few steps in [WT-setup](https://bridgei2i.atlassian.net/wiki/spaces/WAT/pages/2435416069/WT+Setup)
        - update the Ubuntu repository - `sudo apt-get update`
        - install visual studio code - download from [Visual Studio Code - Code Editing Redefined](https://code.visualstudio.com/download)
        - install Git  - `sudo apt-get install git`
        - Install python -  `sudo apt-get install python3` (>v3.6)

    - SafeBuilt Docs - Setup PYTHONPATH variable [Setup PYTHONPATH variable](https://bridgei2i.atlassian.net/wiki/spaces/SAF/pages/1379205136/Setting+PYTHONPATH+variable)

    - Install pip3
        - install pip - `sudo apt-get install python3-pip`


- Suggest to Create a Python Virtual Environment. 
    - This can be done using the required steps mentioned in [WT-setup](https://bridgei2i.atlassian.net/wiki/spaces/WAT/pages/2435416069/WT+Setup)
        > download virtual environment using pip3 :- `sudo pip3 install virtualenv`

        > check the version of virtual env - `virtualenv --version`

        > go to the workspace folder

        > open terminal

        > create virtual environment :- `virtualenv <name of the virtual environment>`
         
        > move into the virtual environment using terminal
        
        > move to bin

        > activate the virtual environment :- `source activate`.

        > Note : To deactivate environment (after running samples)- `source deactivate`




### General Info :

- Help with VSCode Setup for Python - [https://code.visualstudio.com/docs/python/environments](link)

